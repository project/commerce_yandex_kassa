# Yandex Kassa

This is a fork project from [yandex-money-cms-v2-drupal8](https://github.com/yandex-money/yandex-money-cms-v2-drupal8) project as it bears inside the modifications only FOR *Yandex Kassa* Offsite Payment Gateway. It's  

## Requirements
* Drupal 8.7.7+ || 9
* PHP 7.2 +
* libCurl

### About Kassa
Remote API Service for accepting payments by Yandex.

[Yandex Kassa website](http://kassa.yandex.ru/)

#### User Requirements
* Need a VAT Registration,
* Money will be transfered to personal account and then managed thereafter,
* An enstabilished by the contract fee will be applied for all transactions.

First you'll need to [Get an account](https://money.yandex.ru/joinups) and then obtain the **shopId** and **secret key** parameters to use inside configuration.

### Available payment methods
You can choose any or all from below:

* Debit/Credit cards — Visa, Mastercard и Maestro, «Мир»;
* Яндекс.Деньги;

### [RUS] Отправка данных для чеков по 54-ФЗ

Если вы подключите решение Кассы для 54-ФЗ, модуль будет отправлять в Кассу данные для чека вместе с информацией о заказе.
 [Подробности на сайте Кассы](https://kassa.yandex.ru/features) 
